package com.mtechsoft.fleetmanagmentapp.authModels

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("message")
     val message: String?=null,
    @SerializedName("status")
    val status: Int?=null,
    @SerializedName("data") val user: User) {
    // Null default values create a no-argument default constructor, which is needed
    // for deserialization from a DataSnapshot.
}