package com.mtechsoft.fleetmanagmentapp.authModels
import com.google.gson.annotations.SerializedName

data class User (
    @SerializedName("id")
    val user_id: String? = null,

    @SerializedName("fullname")
    val fullname: String? = null,

    @SerializedName("username")
    val username: String? = null,

    @SerializedName("password")
    val password: String? = null,

    @SerializedName("email")
    val email: String? = null,

    @SerializedName("phone")
    val phone: String? = null,

    @SerializedName("type")
    val type: String? = null,

    @SerializedName("updated_on")
    val updated_on: String? = null,


        @SerializedName("status")
    val status: String? = null,

    @SerializedName("failed_login")
    val failed_login: String? = null,

    @SerializedName("created_by")
    val created_by: String? = null,

    @SerializedName("reset_password")
    val reset_password: String? = null,

    @SerializedName("dob")
    val dob: String? = null,

    @SerializedName("gender")
    val gender: String? = null,

    @SerializedName("latitude")
    val latitude: String? = null,

    @SerializedName("longitude")
    val longitude: String? = null,

    @SerializedName("address")
    val address: String? = null,

    @SerializedName("is_online")
    val is_online: String? = null,

    @SerializedName("user_key")
    val user_key: String? = null,




    @SerializedName("created_on")
    val created_on: String? =null) {
    // Null default values create a no-argument default constructor, which is needed
    // for deserialization from a DataSnapshot.
}

