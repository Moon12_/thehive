package com.mtechsoft.fleetmanagmentapp.com.mtechsoft.fleetmanagmentapp.authModels

import com.google.gson.annotations.SerializedName

data class UserLatLngResponse(
    @SerializedName("message")
     val message: String?=null,
    @SerializedName("status")
    val status: Boolean?=null)