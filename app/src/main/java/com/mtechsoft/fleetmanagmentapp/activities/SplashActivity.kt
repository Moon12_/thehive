package com.mtechsoft.fleetmanagmentapp.com.mtechsoft.fleetmanagmentapp.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.mtechsoft.fleetmanagmentapp.R


class SplashActivity : AppCompatActivity() {
    private val progressBar: ProgressBar? = null
    private val pStatus = 0
    private val handler = Handler()
    private val SPLASH_TIME_OUT = 3000L
    lateinit var login_status: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val sharedPreference =
            getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)



        Handler().postDelayed(
            {
                val i = Intent(this@SplashActivity, LandingActivity::class.java)
                startActivity(i)
                finish()
            }, SPLASH_TIME_OUT
        )

    }


}
