package com.mtechsoft.fleetmanagmentapp.com.mtechsoft.fleetmanagmentapp.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mtechsoft.fleetmanagmentapp.auth.AuthVM
import com.mtechsoft.fleetmanagmentapp.com.mtechsoft.fleetmanagmentapp.authModels.UserLatLngResponse
import com.mtechsoft.fleetmanagmentapp.databinding.FragmentMainBinding
import com.mtechsoft.fleetmanagmentapp.fragments.base.BaseFragment
import com.mtechsoft.fleetmanagmentapp.networking.GetDataService
import com.mtechsoft.fleetmanagmentapp.networking.RetrofitClientInstance
import com.mtechsoft.fleetmanagmentapp.sharedPref.SharedPrefHelper
import com.mtechsoft.fleetmanagmentapp.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainFragment : BaseFragment<FragmentMainBinding, AuthVM>() {

    var userId: String = ""
    lateinit var latitude: String
    lateinit var longitude: String
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getSharedPrefence()
        SharedPrefHelper.instance!!.apply {
            userId = getLoginUserData.user_id.toString()

        }
//        callApiUserLatLong()
    }


    override fun getViewModel() = AuthVM::class.java


    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentMainBinding.inflate(inflater, container, false)


    private fun callApiUserLatLong() {

        val service = RetrofitClientInstance.createService(GetDataService::class.java)
        val call = service.userLatLong(userId, latitude, longitude)

        call.enqueue(object : Callback<UserLatLngResponse> {
            override fun onFailure(call: Call<UserLatLngResponse>?, t: Throwable?) {
                hideProgressDialog()
                Utils.showToast(t!!.printStackTrace().toString())
            }

            override fun onResponse(
                call: Call<UserLatLngResponse>?,
                response: Response<UserLatLngResponse>?
            ) {
                hideProgressDialog()
                if (response!!.isSuccessful) {
                    response.body()!!.apply {
                        Toast.makeText(
                            requireContext(),
                            response.body()!!.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()

//                        Utils.navigateAndClearBackStack(
//                            MainActivity::class.java,
//                            requireActivity()
//                        )
                    }
                } else {
                    Toast.makeText(requireContext(), response.message(), Toast.LENGTH_SHORT).show()
                }

            }

        })
    }

    fun getSharedPrefence() {
        val sharedPreference =
            activity?.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        if (sharedPreference != null) {
            latitude = sharedPreference.getString("latitude", null).toString()
            longitude = sharedPreference.getString("longitude", null).toString()
        }
        if (sharedPreference != null) {
            sharedPreference.getLong("l", 1L)
        }
//        showToast(id + user_name)
    }
}