package com.mtechsoft.fleetmanagmentapp.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mtechsoft.fleetmanagmentapp.MainActivity
import com.mtechsoft.fleetmanagmentapp.R
import com.mtechsoft.fleetmanagmentapp.activities.HomeActivity
import com.mtechsoft.fleetmanagmentapp.auth.AuthVM
import com.mtechsoft.fleetmanagmentapp.authModels.UserResponse
import com.mtechsoft.fleetmanagmentapp.databinding.FragmentRegisterBinding
import com.mtechsoft.fleetmanagmentapp.fragments.base.BaseFragment
import com.mtechsoft.fleetmanagmentapp.networking.ApiConstants
import com.mtechsoft.fleetmanagmentapp.networking.GetDataService
import com.mtechsoft.fleetmanagmentapp.networking.RetrofitClientInstance
import com.mtechsoft.fleetmanagmentapp.sharedPref.SharedPrefHelper
import com.mtechsoft.fleetmanagmentapp.utils.BaseView
import com.mtechsoft.fleetmanagmentapp.utils.Constants
import com.mtechsoft.fleetmanagmentapp.utils.Utilities
import com.mtechsoft.fleetmanagmentapp.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RegisterFragment : BaseFragment<FragmentRegisterBinding, AuthVM>(), BaseView {

    var getPassword = ""
    var getUserName = ""
    lateinit var latitude: String
    lateinit var longitude: String
    lateinit var address: String
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setValues()
        setListener()
        getSharedPrefence()
        Toast.makeText(
            requireContext(),
            latitude + longitude,
            Toast.LENGTH_SHORT
        )
            .show()

    }


    override fun getViewModel() = AuthVM::class.java


    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
    ) = FragmentRegisterBinding.inflate(inflater, container, false)


    override fun init() {


    }

    override fun setListener() {
    }

    override fun setValues() {
        binding.apply {

            buttonAuth.setOnClickListener {
                getPassword = binding.editPassword.text.toString()
                getUserName = binding.editUserName.text.toString()
                signUpUser(getUserName, getPassword)
            }
            ivBack.setOnClickListener {
                activity?.finish()
            }
            terms.setOnClickListener {
                terms()
            }
        }


    }

    private fun signUpUser(userName: String, password: String) {

        if (!TextUtils.isEmpty(userName) || !TextUtils.isEmpty(password)) {
            showProgressDialog()
            callApiForSignUp(userName, password, Constants.APP_KEY)

        } else {
            Utils.showSnackBar(view, "Please Fill All Fields")
        }
    }

    private fun callApiForSignUp(userName: String, password: String, appKey: String) {

        val service = RetrofitClientInstance.createService(GetDataService::class.java)
        val call = service.signUp(userName, password, latitude, longitude, appKey)

        call.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>?, t: Throwable?) {
                hideProgressDialog()
                Utils.showToast(t!!.printStackTrace().toString())
            }

            override fun onResponse(call: Call<UserResponse>?, response: Response<UserResponse>?) {
                hideProgressDialog()
                if (response!!.isSuccessful) {
                    val loginResponse = response.body()!!.user
                    response.body()!!.apply {
                        Toast.makeText(
                            requireContext(),
                            response.body()!!.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
//                        (context as MainActivity).navController.navigate(R.id.action_registerFragment_to_mainFragment)
                        Utilities.saveString(activity,
                            Utilities.USER_ID,
                            loginResponse.user_id.toString())
                        Utilities.saveString(activity, Utilities.USER_NAME, loginResponse.fullname)
                        Utilities.saveString(activity, Utilities.USER_LAT, loginResponse.latitude)
                        Utilities.saveString(activity, Utilities.USER_LNG, loginResponse.longitude)
                        startActivity(Intent(activity, HomeActivity::class.java))

                        SharedPrefHelper.instance!!.apply {
                            saveLoginState(true)
                            saveLoginUser(user)
                        }
//                        Utils.navigateAndClearBackStack(
//                            MainActivity::class.java,
//                            requireActivity()
//                        )
                    }
                } else {
                    Toast.makeText(requireContext(), response.message(), Toast.LENGTH_SHORT).show()
                }

            }

        })
    }

    fun getSharedPrefence() {
        val sharedPreference =
            activity?.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        if (sharedPreference != null) {
            latitude = sharedPreference.getString("latitude", null).toString()
            longitude = sharedPreference.getString("longitude", null).toString()
            address = sharedPreference.getString("address", null).toString()
        }
        if (sharedPreference != null) {
            sharedPreference.getLong("l", 1L)
        }
//        showToast(id + user_name)
    }

    fun terms() {
        val url = "https://www.termsandconditionsgenerator.com/"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }
}