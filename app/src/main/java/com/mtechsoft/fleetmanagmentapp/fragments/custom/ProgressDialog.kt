package com.mtechsoft.fleetmanagmentapp.fragments.custom

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.mtechsoft.fleetmanagmentapp.databinding.LoadingDialogBinding

class ProgressDialog : DialogFragment() {

    private var _binding: LoadingDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LoadingDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog?.setCancelable(false)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
