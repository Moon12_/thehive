@file:Suppress("DEPRECATION")

package com.mtechsoft.fleetmanagmentapp.utils

import com.mtechsoft.fleetmanagmentapp.MyApp.Companion.context
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color.RED
import android.graphics.Color.red
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.Transformation
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentActivity
import androidx.viewbinding.BuildConfig.DEBUG
import coil.load
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.snackbar.Snackbar
import com.mtechsoft.fleetmanagmentapp.R
import com.mtechsoft.fleetmanagmentapp.fragments.custom.ProgressDialog
import de.hdodenhof.circleimageview.CircleImageView
import java.io.IOException
import java.io.InputStream
import java.math.BigDecimal
import java.nio.charset.StandardCharsets
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
object Utils {


    @SuppressLint("SimpleDateFormat")
    fun changeDateFormat(date: String?): String? {
//        String date="Mar 10, 2016 6:30:00 PM";
        var date = date
        @SuppressLint("SimpleDateFormat") var spf = SimpleDateFormat(DATE_FORMAT)
        val newDate: Date?
        return try {
            newDate = spf.parse(date)
            spf = SimpleDateFormat("dd MMM yyyy")
            if (DEBUG && newDate == null) {
                error("Assertion failed")
            }
            date = spf.format(newDate)
            date
        } catch (e: ParseException) {
            e.printStackTrace()
            ""
        }
    }

    fun goToNextScreenWithFinish(cls: Class<*>?, ct: Context) {
        val intent = Intent(ct, cls)
        ct.startActivity(intent)
        (ct as Activity).finish()
    }

    fun isEmailValid(email: String?): Boolean {
        return !Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun goToNextScreenWithoutFinish(cls: Class<*>?, ct: Context) {
        val intent = Intent(ct, cls)
        ct.startActivity(intent)
    }

    const val DATE_FORMAT = "E MMM d, yyyy"

    var dateFormat = SimpleDateFormat("E MMM d, yyyy")
    fun getJsonFromAssets(context: Context, fileName: String?): String? {
        return try {
            val `is` = context.assets.open(fileName!!)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer, StandardCharsets.UTF_8)
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
    }

    private fun formatTimeStamp(time: Long): String? {
        var time = time
        val SECOND_MILLIS = 1000
        val MINUTE_MILLIS = 60 * SECOND_MILLIS
        val HOUR_MILLIS = 60 * MINUTE_MILLIS
        val DAY_MILLIS = 24 * HOUR_MILLIS
        if (time < 1000000000000L) {
            time *= 1000
        }
        val now = System.currentTimeMillis()
        if (time > now || time <= 0) {
            return null
        }
        val diff = now - time
        return when {
            diff < MINUTE_MILLIS -> {
                "Just now"
            }
            diff < 2 * MINUTE_MILLIS -> {
                "A minute ago"
            }
            diff < 50 * MINUTE_MILLIS -> {
                "$diff / $MINUTE_MILLIS minutes ago"
            }
            diff < 90 * MINUTE_MILLIS -> {
                "An hour ago"
            }
            diff < 24 * HOUR_MILLIS -> {
                "$diff / $HOUR_MILLIS hours ago"
            }
            diff < 48 * HOUR_MILLIS -> {
                "Yesterday"
            }
            else -> {
                "$diff / $DAY_MILLIS  days ago"
            }
        }
    }

    /*  Picasso
    fun setImageWithPb(imgUrl: String?, ivImage: ShapeableImageView?, pbLoading: ProgressBar) {

         Picasso.get()
                 .load(imgUrl)
                 .error(R.drawable.place_holder)
                 .into(ivImage, object : Callback {
                     override fun onSuccess() {
                         pbLoading.visibility = View.GONE
                     }

                     override fun onError(e: Exception) {
                         pbLoading.visibility = View.GONE
                     }
                 })
     }
*/
    fun setImageWithPbCoil(
        imgUrl: String?,
        ivImage: ShapeableImageView?,
        pbLoading: ProgressBar
    ) {
        ivImage!!.load(imgUrl) {
            target(
                onStart = { placeholder ->
                    pbLoading.visibility = View.VISIBLE
                    // Handle the placeholder drawable.
                },
                onSuccess = { result ->
                    pbLoading.visibility = View.GONE
                    ivImage.load(result)
                    // Handle the successful result.
                },
                onError = { error ->
                    // Handle the error drawable.
                }
            )
//                size(width = 250, height = 300)

//                    transformations(CircleCropTransformation())
        }
    }

    fun setImageWithCoil(
        imgUrl: String?,
        ivImage: CircleImageView?
    ) {
        ivImage!!.load(imgUrl) {
            target(
                onStart = { placeholder ->
                    // Handle the placeholder drawable.
                },
                onSuccess = { result ->
                    ivImage.load(result)
                    // Handle the successful result.
                },
                onError = { error ->
                    // Handle the error drawable.
                }
            )
//                size(width = 250, height = 300)

//                    transformations(CircleCropTransformation())
        }
    }

    fun getBitmapFromAsset(strName: String?): Bitmap {
        val assetManager = context.assets
        var istr: InputStream? = null
        try {
            istr = assetManager.open(strName!!)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return BitmapFactory.decodeStream(istr)
    }

    fun makeACall(ct: Context, phoneNumber: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNumber")
        ct.startActivity(intent)
    }

//    fun getRandomColorId(): Int {
//        val androidColors = context.resources.getIntArray(R.array.rainbow)
//        return androidColors[Random().nextInt(androidColors.size)]
//    }

//    fun getRandomFrameId(): Int {
//        val frameList = listOf(
//            R.drawable.frame_1,
//            R.drawable.frame_2,
//            R.drawable.frame_3,
//            R.drawable.frame_4,
//            R.drawable.frame_5
//        )
////            val androidColors = context.resources.getIntArray(R.array.rainbow)
//        return frameList[Random().nextInt(frameList.size)]
//    }

//    fun navigateToDirection(ct: Context, currentLatLng: LatLng, destinationLatLng: LatLng) {
//        val intent = Intent(
//            Intent.ACTION_VIEW,
//            Uri.parse("http://maps.google.com/maps?saddr=" + currentLatLng.latitude + "," + currentLatLng.longitude + "&daddr=" + destinationLatLng.latitude + "," + destinationLatLng.longitude)
//        )
//        ct.startActivity(intent)
//    }

    //Formatting TimeStamp to 'EEE MMM dd yyyy (HH:mm:ss)'
    //Input  : 2018-05-23 9:59:01
    //Output : Wed May 23 2018 (9:59:01)
    fun formateDate(dateStr: String?): String {
        try {
//            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Mon Dec 21, 2020
            @SuppressLint("SimpleDateFormat")
            val fmt = SimpleDateFormat("EEE MMM dd, yyyy")
            val date = fmt.parse(dateStr)
            @SuppressLint("SimpleDateFormat")
            val fmtOut = SimpleDateFormat("yyyy-MM-dd ")
            return fmtOut.format(date)
        } catch (e: ParseException) {
        }
        return ""
    }

    fun reverseDateFormat(dateStr: String?): String {
        try {
//            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Mon Dec 21, 2020
            @SuppressLint("SimpleDateFormat") val fmt = SimpleDateFormat("yyyy-MM-dd")
            val date = fmt.parse(dateStr)
            @SuppressLint("SimpleDateFormat") val fmtOut = SimpleDateFormat("EEE MMM dd, yyyy")
            return fmtOut.format(date)
        } catch (e: ParseException) {
        }
        return ""
    }

    fun roundDecimal(d: Float, decimalPlace: Int): BigDecimal {
        var bd = BigDecimal(java.lang.Float.toString(d))
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP)
        return bd
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun changeStatusBarColor(statusBarColor: Int, myActivityReference: Activity) {
        val window = myActivityReference.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = statusBarColor
    }

    val currentDate: String
        @SuppressLint("SimpleDateFormat")
        get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
            current.format(formatter)
        } else {
            val date = Date()
            val formatter = SimpleDateFormat(DATE_FORMAT)
            formatter.format(date)
        }

    //2020-05-09
    fun selectDate(ct: Context?, etDate: MaterialButton) {
        val myCalendar = Calendar.getInstance()
        val date =
            OnDateSetListener { view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                // TODO Auto-generated method stub
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                updateLabel(etDate, myCalendar)
            }
        android.app.DatePickerDialog(
            ct!!, date, myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH],
            myCalendar[Calendar.DAY_OF_MONTH]
        ).show()
    }


    fun updateLabel(et: MaterialButton, myCalendar: Calendar) {
        val sdf = SimpleDateFormat(DATE_FORMAT, Locale.US)
        et.text = sdf.format(myCalendar.time)
    }

    fun getImageUri(ct: Context, inImage: Bitmap?): Uri {
        val resized = Bitmap.createScaledBitmap(inImage!!, 720, 720, false)
        val path = MediaStore.Images.Media.insertImage(
            ct.contentResolver,
            resized,
            "Title",
            null
        )
        return Uri.parse(path)
    }

    fun navigateAndClearBackStack(cls: Class<*>?, ct: Context) {
        val intent = Intent(ct, cls)
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        ct.startActivity(intent)
        (ct as Activity).finish()
    }

    private const val TAG_PROGRESS_DIALOG = "progress_dialog"
    private var progressDialog: ProgressDialog? = null
    fun showProgressDialog(activity: FragmentActivity) {
        if (progressDialog == null) {
            progressDialog = ProgressDialog()
        }
        progressDialog?.let {
            if (!it.isVisible) {
                it.show(activity.supportFragmentManager, TAG_PROGRESS_DIALOG)
            }
        }
    }

    fun hideProgressDialog() = progressDialog?.dismiss()

    fun showSnackBar(view: View?, msg: String?) {
        //findViewById(android.R.id.content)
        Snackbar.make(view!!, msg!!, Snackbar.LENGTH_SHORT)
            .show()
    }




    @RequiresApi(Build.VERSION_CODES.M)
    fun showSnackBarError(view: View?, msg: String?) {
        //findViewById(android.R.id.content)
        Snackbar.make(view!!, msg!!, Snackbar.LENGTH_SHORT)
            .setAction("Ok") {
            }
            .setBackgroundTint(context.resources.getColor(R.color.red, null))
            .show()
    }

    fun isPasswordLengthGreaterThan5(password: String): Boolean {
        return password.length <= 5
    }

    fun setVisibility(visible: View, gone: View) {
        visible.visibility = View.VISIBLE
        gone.visibility = View.GONE
    }

    fun showToast(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }

    @Suppress("DEPRECATION")
    fun getSpannedText(text: String): Spanned? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT);
        } else {
            Html.fromHtml(text);
        }
    }

    fun showMaterialDialog(context: Context, title: String?, message: String?) {
        MaterialAlertDialogBuilder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(context.resources.getString(R.string.ok)) { dialog, which ->
                // Respond to positive button press
                dialog.dismiss()
            }
            .show()
    }


    fun showShortToast(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    private fun isInternetAvailable(): Boolean {
        var result = false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }

        return result
    }

    /*public static boolean isEmpty(TextInputEditText et) {

    return TextUtils.isEmpty(getText(et).trim());
}*/
    fun isEmpty(tv: TextView): Boolean {
        return TextUtils.isEmpty(getText(tv).trim { it <= ' ' })
    }

    fun isEmpty(et: EditText): Boolean {
        return TextUtils.isEmpty(getText(et))
    }

    fun isEmpty(et: AutoCompleteTextView): Boolean {
        return TextUtils.isEmpty(getText(et))
    }

    fun getText(et: EditText): String {
        return et.text.toString().trim { it <= ' ' }
    }

    fun getText(et: AutoCompleteTextView): String {
        return et.text.toString().trim { it <= ' ' }
    }

    fun getText(tv: TextView): String {
        return tv.text.toString().trim { it <= ' ' }
    }

    fun getText(button: MaterialButton): String {
        return button.text.toString().trim { it <= ' ' }
    }

    fun animate(view: View, animId: Int) {
        val animation1 = AnimationUtils.loadAnimation(context, animId)
        view.startAnimation(animation1)
    }

    fun animateArrow(view: View) {
        view.animate().rotationBy(180F).setDuration(200).start()
    }

    fun expand(v: View) {
        val matchParentMeasureSpec =
            View.MeasureSpec.makeMeasureSpec((v.parent as View).width, View.MeasureSpec.EXACTLY)
        val wrapContentMeasureSpec =
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height =
                    if (interpolatedTime == 1f) ViewGroup.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // Expansion speed of 1dp/ms
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toLong()
        v.startAnimation(a)
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight
        val a: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height =
                        initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // Collapse speed of 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toLong()
        v.startAnimation(a)
    }

}