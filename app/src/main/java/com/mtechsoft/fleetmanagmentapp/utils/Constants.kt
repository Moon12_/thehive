package com.mtechsoft.fleetmanagmentapp.utils


object Constants {
    object API {
        const val PHOTOS_PER_PAGE = 30
        const val GUEST_LOGIN = "guestLogin"
        const val LOG_IN = "login"
        const val SOCIAL_LOGIN = "socialLogin"
        const val SIGN_UP = "signUp"
        const val CREATE_PAYMENT = "createPaymentIntent"
        const val ANNUAL_SUB = "annualSubscription"
        const val PURCHASE_BOOK = "purchaseBook"
        const val BOOKING_SESSION = "bookingSchedule"
        const val GET_USERS = "getUsers"
    }

    object AuthType {
        const val APP = "app_login"
        const val FB = "fb"
        const val GOOGLE = "gmail"
        const val GUEST = "guest_login"

    }
    const val APP_KEY = "55ba79d5b0dccb9a712016fbbcffc7b899f85a57"

    const val KEY = "key"
    const val CHECK_OUT_TITLE = "check_out_title"
    const val CHECK_OUT_DESC = "check_out_desc"
    const val CHECK_OUT_CHARGES = "check_out_charges"

    const val TERMS_CONDITION_LINK = "https://www.shayyourlovediva.com/terms-and-conditions"
    const val CONTACT_US = "https://www.shayyourlovediva.com/contact"
    const val ABOUT_US = "https://www.shayyourlovediva.com/about"
    const val PRIVACY_POLICY = "https://www.shayyourlovediva.com/privacy-policy"
    const val CHECK_OUT_LINK = "https://www.divauniversity.org/offers/F2PLj4kN/checkout"
    const val POWERFUL_EXERCISE_LINK = "https://shaylevister714.lpages.co/findmelovenow/"
    const val COACHING_DETAIL = "COACHING_DETAIL"
    const val BOOKING_BUNDLE = "BOOKING_BUNDLE"
    const val BOOKING_DATE = "BOOKING_DATE"
    const val BOOKING_TIME = "BOOKING_TIME"
    const val BOOK = "BOOK"
    const val BOOK_LINK = "https://drive.google.com/file/d/1uavYcZhxEHLX_GUbJcze2nHXN388rjO4/view"
    const val MP3_LINK =
        "https://res.cloudinary.com/dgq11ggb9/video/upload/v1613777665/Balancing_Your_Masculine_And_Feminine_Energy_Meditation_zhh1ke.mp3"
}