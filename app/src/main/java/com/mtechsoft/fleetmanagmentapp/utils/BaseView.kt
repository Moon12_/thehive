package com.mtechsoft.fleetmanagmentapp.utils


interface BaseView {
    fun init()
    fun setListener()
    fun setValues()
}