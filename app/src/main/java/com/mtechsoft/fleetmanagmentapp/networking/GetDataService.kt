package com.mtechsoft.fleetmanagmentapp.networking

import com.mtechsoft.fleetmanagmentapp.authModels.UserResponse
import com.mtechsoft.fleetmanagmentapp.com.mtechsoft.fleetmanagmentapp.authModels.UserLatLngResponse
import com.mtechsoft.fleetmanagmentapp.networking.ApiConstants
import retrofit2.Call
import retrofit2.http.*

interface GetDataService {

    @FormUrlEncoded
    @POST(ApiConstants.Api.LOGIN)
    fun signUp(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("lat") lat: String,
        @Field("lng") lng: String,
        @Field("app_key") app_key: String,
//        @Field("user_password") user_password: String,
    ): Call<UserResponse>

    @FormUrlEncoded
    @POST(ApiConstants.Api.userLatLong)
    fun userLatLong(
        @Field("user_id") user_id: String,
        @Field("lati") lati: String,
        @Field("longi") longi: String,
    ): Call<UserLatLngResponse>


}