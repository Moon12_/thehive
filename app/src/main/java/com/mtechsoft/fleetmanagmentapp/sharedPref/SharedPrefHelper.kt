package com.mtechsoft.fleetmanagmentapp.sharedPref
import com.mtechsoft.fleetmanagmentapp.authModels.User


class SharedPrefHelper {
    //login preferences
    fun saveDeviceToken(value: String?) {
        SharedPref.instance!!.saveValue(DEVICE_TOKEN, value)
    }

    val deviceToken: String
        get() = SharedPref.instance!!.getStringValue(DEVICE_TOKEN)!!

    fun saveIntegerValue(value: Int) {
        SharedPref.instance!!.saveValue(ABC_VALUE, value)
    }
    fun saveStringValue(value: String?){
        SharedPref.instance!!.saveValue(ABC_VALUE,value)
    }


    val integerValue: Int
        get() = SharedPref.instance!!.getIntValue(ABC_VALUE)

    val stringValue: String?
        get() = SharedPref.instance!!.getStringValue(ABC_VALUE)

//    fun saveGuestLogin(isTrue: Boolean) {
//        SharedPref.instance!!.saveValue(Constants.API.GUEST_LOGIN, isTrue)
//    }

    //Guest Login preferences
//    val isGuestLoggedIn: Boolean
//        get() = SharedPref.instance!!.getBoolValue(Constants.API.GUEST_LOGIN)


    fun saveLoginState(isTrue: Boolean) {
        SharedPref.instance!!.saveValue(IS_LOGIN, isTrue)
    }
    fun saveParticipatedState(isTrue: Boolean) {
        SharedPref.instance!!.saveValue(IS_PARTICIPATED, isTrue)
    }

    //login preferences
    val isLoggedIn: Boolean
        get() = SharedPref.instance!!.getBoolValue(IS_LOGIN)
    //participated preferences
    val isParticipated: Boolean
        get() = SharedPref.instance!!.getBoolValue(IS_PARTICIPATED)


    fun saveLoginUser(user: User) {
        SharedPref.instance!!.saveLoginData(LOGIN_DATA, user)
    }

    val getLoginUserData: User
        get() = SharedPref.instance!!.getLoginData(LOGIN_DATA)


    companion object {
        //add key here
        private const val LOGIN_TYPE = "LOGIN_TYPE"
        private const val ABC_VALUE = "ABC_VALUE"
        private const val IS_LOGIN = "IS_LOGIN"
        private const val IS_PARTICIPATED = "IS_PARTICIPATED"
        private const val LOGIN_DATA = "LOGIN_DATA"
        private const val PART_DATA = "PART_DATA"
        private const val VRACE_DATA = "VRACE_DATA"
        private const val SOCIAL_DATA = "SOCIAL_DATA"
        private const val RUN_DATA = "RUN_DATA"
        private const val DEVICE_TOKEN = "DEVICE_TOKEN"
        var instance: SharedPrefHelper? = null
            get() {
                if (field == null) {
                    synchronized(SharedPrefHelper::class.java) {
                        if (field == null) {
                            field = SharedPrefHelper()
                        }
                    }
                }
                return field
            }
            private set
    }
}