package com.mtechsoft.fleetmanagmentapp.sharedPref

import com.mtechsoft.fleetmanagmentapp.MyApp.Companion.context
import android.content.Context
import com.google.gson.Gson
import com.mtechsoft.fleetmanagmentapp.authModels.User


class SharedPref  //Private Constructor
private constructor() {
    private val sharedPreferences = context.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
    private val editor = sharedPreferences.edit()
    fun saveValue(key: String?, value: String?) {
        editor.putString(key, value)
        editor.apply()
    }

    fun saveValue(key: String?, value: Int) {
        editor.putInt(key, value)
        editor.apply()
    }

    fun saveValue(key: String?, value: Boolean) {
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getStringValue(key: String?): String? {
        return sharedPreferences.getString(key, "")
    }

    fun getIntValue(key: String?): Int {
        return sharedPreferences.getInt(key, -1)
    }

    fun clearSharedPref() {
        editor.clear().apply()
    }

    fun getBoolValue(key: String?): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

    //
    fun getLoginData(key: String?): User {
        val gson = Gson()
        val json = sharedPreferences.getString(key, null)
        return gson.fromJson(json, User::class.java)

    }


    fun saveLoginData(key: String?, user: User) {
        val gson = Gson()
        val json = gson.toJson(user)
        editor.putString(key, json);
        editor.apply();
    }




    /*public void (String key,  ) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(key,json);
        editor.apply();
    }

    //this method will give the logged in user
    public  (String key) {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key,null);
        return gson.fromJson(json, User.class);
    }*/

    companion object {
        private const val SHARED_PREF = "WMC"
        private var mInstance: SharedPref? = null

        @get:Synchronized
        val instance: SharedPref?
            get() {
                if (mInstance == null) {
                    mInstance = SharedPref()
                }
                return mInstance
            }
    }




}