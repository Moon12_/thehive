package com.mtechsoft.fleetmanagmentapp
import android.app.Application
import android.content.Context
import androidx.core.app.NotificationManagerCompat
//import com.shaybettercoaching.utils.NotificationHelper
//import com.stripe.android.PaymentConfiguration

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
//
//        NotificationHelper.createNotificationChannel(this,
//            NotificationManagerCompat.IMPORTANCE_DEFAULT, false,
//            getString(R.string.app_name), "App notification channel.")
//
//        PaymentConfiguration.init(
//            applicationContext,
//            context.getString(R.string.STRIPE_TEST_KEY)
//        )

    }

    companion object {

        @JvmStatic
        var instance: MyApp? = null
            private set

        @JvmStatic
        val context: Context
            get() = instance!!.applicationContext
    }

}